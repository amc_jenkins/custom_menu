//All contants from Version.cs are visible for test projects. It is normal to disable this warning
#pragma warning disable 0436

using System.Reflection;

[assembly: AssemblyVersion(Constants.ASSEMBLY_VERSION)]
[assembly: AssemblyFileVersion(Constants.ASSEMBLY_VERSION)]
[assembly: AssemblyCompany(Constants.AUTHOR_NAME)]
[assembly: AssemblyCopyright(Constants.COPYRIGHT)]

static class Constants
{
    internal const string ASSEMBLY_VERSION = "1.2.8.9";
    internal const string AUTHOR_NAME = "Flux Factory, Inc.";
    internal const string DESCRIPTION = "`Flux Pipe` solves data interchange between, and synchronous communication with disparate design & engineering programs.";
    internal const string COPYRIGHT = "Copyright © Flux Factory, Inc. 2016";
}