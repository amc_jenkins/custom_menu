window.ysy = window.ysy || {};
ysy.proManager = ysy.proManager || {};
ysy.pro = ysy.pro || {};
$.extend(ysy.proManager, {
  pros: [],
  patch: function () {
    window.ysy = window.ysy || {};
    ysy.settings = ysy.settings || {};
    for (var key in ysy.pro) {
      if (!ysy.pro.hasOwnProperty(key)) continue;
      if (ysy.pro[key].patch) {
        this.pros.push(ysy.pro[key]);
      }
    }

    this.forEachPro(function () {
      this.patch()
    }, "patch");

    ysy.view.Main.prototype.initProToolbars = function (ctx) {
      ysy.proManager.forEachPro(function () {
        this.initToolbar(ctx)
      }, "initToolbar");
    };
  },
  forEachPro: function (func, funcName) {
    for (var i = 0; i < this.pros.length; i++) {
      var pro = this.pros[i];
      if (funcName === undefined || pro[funcName]) {
        $.proxy(func, pro)();
      }
    }
  },
  getConfig: function () {
    var config = {};
    this.forEachPro(function () {
      var featureConfig = this.getConfig();
      if (featureConfig) {
        $.extend(config, featureConfig);
      }
    }, "getConfig");
    return config;
  },
  getTaskClass: function (task) {
    var css = "";
    this.forEachPro(function () {
      var featureCss = this.getTaskClass(task);
      if (featureCss) {
        css += featureCss;
      }
    }, "getTaskClass");
    return css;
  },
  showHelp: function () {
    var div = $(this).next();
    var x = div.clone().attr({"id": div[0].id + "_popup"}).appendTo($("body"));
    showModal(x[0].id);
  },
  closeAll: function (except) {
    this.forEachPro(function () {
      if (except !== this) this.close();
    }, "close");
  }
});