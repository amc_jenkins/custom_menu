// Case-insensetive search for jQuery >= 1.8
// jQuery.expr[":"].Contains = jQuery.expr.createPseudo(function(arg) {
//     return function( elem ) {
//         return jQuery(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
//     };
// });

// Case-insensetive search for jQuery < 1.8
// jQuery.expr[':'].Contains = function(elem, index, match) {
//   return jQuery(elem).text().toUpperCase().indexOf(match[3].toUpperCase()) >= 0;
// };


RMPlus.Tech = (function (my) {
  var my = my || {};

  my.upd_header = function () {
    $.ajax({ url: '/uc_periods/update_msg_head',
            type: 'get',
            data: $('#uc_period_form').serialize() });
  }

  my.show_warn_if_needed = function () {
    if (typeof uc_warn_hash != 'undefined' && localStorage['uc_warn_hash'] != uc_warn_hash) {
        localStorage['uc_warn_hash'] = uc_warn_hash;
        localStorage['uc_warn_state'] = 'open';
    }

    if (localStorage['uc_warn_state'] != 'closed') {
      var warn_note = $('#uc_warn_note');
      var uc_warn_width = warn_note.outerWidth();
      warn_note.css('width', warn_note.width()+'px');
      warn_note.css('height', warn_note.height()+'px');
      // warn_note.css('top', 3 + 'px');
      // warn_note.css('left', jQuery(window).width() - 3 - uc_warn_width + 'px');
      warn_note.find('.uc_warn_wrapper').hide();
      warn_note.show();
      warn_note.effect('slide', {direction: 'up'} , 300, function () { $('.uc_warn_wrapper').show(); $('#uc_warn_note').css('width', 'auto').css('height', 'auto')});
    }
  }

  return my;
})(RMPlus.Tech || {});


$(document).ready(function () {

  $('#uc_period_form :input').change(function () {
    RMPlus.Tech.upd_header();
  });

  $('a.uc_preview').click(function () {
    $.ajax({ url: '/uc_periods/preview',
             dataType: 'script',
              type: 'post',
              data: {preview_text: $('#'+$(this).attr('data-obj')).val()} });

  });

  $(document.body).on('click', '.uc_close_warning', function () {
    $('#uc_warn_note').hide();
    localStorage['uc_warn_state'] = 'closed';
  });

  $(document.body).on('change', '.uc-controller', function(event) {
    var $select = $(event.target);
    var $actions_select = $select.siblings('select').first();
    var controller = $select.val();
    var action_options = $('#all_actions optgroup[label='+ controller +']').children().clone();
    $actions_select.children().filter(function(index){
      return this.innerHTML !== "&nbsp;";
    }).remove();
    $actions_select.select2("val", "");
    $actions_select.append(action_options);
  });

  RMPlus.Tech.show_warn_if_needed();
});
